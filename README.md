# Longan_Vision_Smart_Helmet_Platform_Development

IT-17618


Longan Vision Development - Steps to setup LV git project
Please install ubuntu version 18.04 on the pc

## Clone and Build LV with Variscite

```
$ git config --global user.name "Your Name"
$ git config --global user.email "Your Email"
```
```
$ mkdir var-fsl-yocto
$ cd ~/var-fsl-yocto
$ git clone git@gitlab.volansys.com:gitrepo/longan_vision_smart_helmet_platform_development.git -b development sources
$ git checkout development
$ cd sources
$ ./lv_source_setup.sh
```

###### NOTE: All the above mentioned commands shall be run for one time.


## Source Build Steps
```
$ MACHINE=imx8mm-var-dart DISTRO=fsl-imx-xwayland . var-setup-release.sh -b build_xwayland
$ cd ..
$ source setup-environment build_xwayland
$ bitbake fsl-image-gui
```


## Making changes to your source code
```
Then you can create your own branch using below commands.
$ git branch <your_branch_name>       ( Note: Branch name shall be "Feature-name" not on your own name)
$ git checkout <your_branch_name>
```

### Add your patches here and then push your changes using following command
```
$ git push git@gitlab.volansys.com:gitrepo/longan_vision_smart_helmet_platform_development.git -b <your_branch_name>
```
To Merge branch, You can login to volansys.gitlab.com and go to respective project and generate the merge request.

NOTE: Make sure when you are pulling the changes before commiting the changes and push your changes regularly.
